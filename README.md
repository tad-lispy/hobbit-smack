# Smack a Hobbit in a Random Part

A funny(?) and cruel command line program in Clojure. Just a learning exercise for me.

## Installation

Clone from here and run.

## Usage

Just:

```sh
lein run
```

### Bugs

What bugs?

## License

Copyright © 2018 Tad Lispy

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
