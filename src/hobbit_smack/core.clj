(ns hobbit-smack.core
  (:gen-class))

(def asym-hobbit-body-parts [{:name "head", :size 3}
                             {:name "left-eye", :size 1}
                             {:name "left-ear", :size 1}
                             {:name "mouth", :size 1}
                             {:name "nose", :size 1}
                             {:name "neck", :size 2}
                             {:name "left-shoulder", :size 2}
                             {:name "left-upper-arm", :size 3}
                             {:name "chest", :size 5}
                             {:name "back", :size 8}
                             {:name "left-forearm", :size 3}
                             {:name "abdomen", :size 2}
                             {:name "left-kidney", :size 1}
                             {:name "left-hand", :size 2}
                             {:name "left-knee", :size 2}
                             {:name "left-thigh", :size 1}
                             {:name "left-lower-leg", :size 3}
                             {:name "left-achilles", :size 1}
                             {:name "left-foot", :size 2}])
(defn matching-body-part
  "Given a left- body part, returns a matching right- part. If the part is not left-, then returns the given part unmodified."
  [part]
  {:name (clojure.string/replace (part :name) #"^left-" "right-")
   :size (part :size)})

(def hobbit-body-parts
  (reduce (fn [parts part] (into parts (set [part (matching-body-part part)])))
          []
          asym-hobbit-body-parts))

(defn -main
  "Hit a random part of the hobbit's body"
  [& args]
  (let [size (reduce + (map :size hobbit-body-parts))
        where (rand size)
        part (loop [[part & remaining] hobbit-body-parts
                    start 0]
               (if (< where (+ start (:size part)))
                 part
                 (recur remaining (+ start (:size part)))))]
    (println (str "SMACK! Hobbit hit in the "
                  (:name part)
                  "! It's worth "
                  (/ size (:size part))
                  " points."))))
